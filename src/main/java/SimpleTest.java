import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SimpleTest {

    public static void main (String[] args){

        //System.setProperty("webdriver.chrome.driver", "/home/ariel/POC/chromedriver");
        //WebDriver driver = new ChromeDriver();
        System.setProperty("webdriver.gecko.driver", "/home/ezequiel/Downloads/automationcourse/geckodriver/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");
        System.out.println(driver.getTitle());
        driver.quit();
    }
}
